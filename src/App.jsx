import { useState } from 'react';
import QuoteBox from './Components/QuoteBox';
import quotes from './json/quotes.json'
import './style.css'

function App() {
  const randomElementFromArray = (arr) => {
    const indexRandom = Math.floor(Math.random() * (arr.length));
    return arr[indexRandom];
  }

  const [quoteRandom, setQuoteRandom] = useState(randomElementFromArray(quotes))
  
  const handleClick = () => {
    setQuoteRandom(randomElementFromArray(quotes));
    setColorRandom(randomColor());
  }

  const randomColor = () => {
    let simbolos = "0123456789ABCDEF";
    let color = "#";

    for (let i = 0; i < 6; i++) {
      color = color + simbolos[Math.floor(Math.random() * 16)];
    }

    return color;
  }

  const [colorRandom, setColorRandom] = useState(randomColor())

  return (
    <div className="App">
      <QuoteBox quoteRandom={quoteRandom} handleClick={handleClick} colorRandom={colorRandom} />
    </div>
  )
}

export default App