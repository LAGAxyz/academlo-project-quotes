import React from 'react'

const ButtonBox = ({colorRandom, handleClick}) => {
  return (
    <button onClick={handleClick} style={{backgroundColor: colorRandom}}>&#62;</button>
  )
}

export default ButtonBox