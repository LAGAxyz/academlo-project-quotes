import React from 'react'
import ButtonBox from './ButtonBox';

const QuoteBox = ({ quoteRandom, handleClick, colorRandom }) => {
    document.body.style.background = colorRandom;

    return (
        <article>
            <i className='bx bxs-quote-left' style={{color:colorRandom}}></i>
            <p style={{color:colorRandom}}>{quoteRandom.quote}</p>
            <small style={{color:colorRandom}}>{quoteRandom.author}</small>
            <ButtonBox colorRandom={colorRandom} handleClick={handleClick}/>
        </article>
    )
}

export default QuoteBox